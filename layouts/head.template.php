<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="Description" content="We work together to create a better tomorrow. Your donations allow Solmar Foundation to work hand-in-hand with several non-government agencies including orphanages, senior centers and shelters in Baja California Sur." />
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Keywords" content="solmar foundation,solmar,donations,ong,Foundation" />
<meta name=”distribution” content=”global” />
<title> <?= $title ?> </title>
<link rel="stylesheet" href="<?=BASE_PATH?>static/css/bootstrap.min.css" crossorigin="anonymous">
<link rel="stylesheet" href="<?=BASE_PATH?>static/css/style.css">
<script src="https://kit.fontawesome.com/4823cf7db0.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?=BASE_PATH?>static/js/jquery-3.4.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>