<header id="header">
    <div class="green-bar">
        <div class="container">
            <div class="social-media">
                <a href="https://www.facebook.com/SolmarFoundation" target="_blank">
                    <span><i class="fab fa-facebook-f"></i></span>
                </a>
                <a href="https://twitter.com/FundacionSolmar" target="_blank">
                    <span><i class="fab fa-twitter"></i></span>
                </a>
            </div>
            <div class="buttons-header">
                <div class="translate-button">
                    <form method="POST">
                        <button name="lang" value=<?= $lang["languaje_translade_abr"] ?> type="submit"><?= $lang["languaje_translade"] ?></button>
                    </form>
                </div>
                <div class="donate-button">
                    <a href="<?= BASE_PATH ?>donate">
                        <?= $lang["donate_menu"] ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar-header container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="<?= BASE_PATH ?>home">
                <img src="<?= BASE_PATH ?>static/img/logos/solmar_fundation.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="<?= BASE_PATH ?>work"><?= $lang["work_menu"] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= BASE_PATH ?>help-us-help"><?= $lang["help_menu"] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= BASE_PATH ?>helping-hands"><?= $lang["helping_hands_menu"] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= BASE_PATH ?>blogs"><?= $lang["blogs_menu"] ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= BASE_PATH ?>connect"><?= $lang["connect_menu"] ?></a>
                    </li>
                    <?php if (isset($_SESSION['usuario'])): ?>
                    <li class="nav-item">
                    <form action="../controllers/AuthController.php" method="POST">
                    <input type="hidden" name="token" value="<?= $_SESSION['token'] ?>">
                    <input type="hidden" name="action" id="action" value="logout">
                        <button type="submit" class="nav-link"><?= $lang["logout_menu"]?></button>
                    </form>
                    </li>
                    <?php endif ?>
                </ul>
            </div>
        </nav>
    </div>
</header>