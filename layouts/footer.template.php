<footer id="footer">
    <section class="footer-sections">
        <section class="container first-footer-section">
            <div class="collapse-sections">
                <div class="collapsed-item newsletter-container">
                    <div class="collapse-element-title visible-xs collapsed" data-toggle="collapse" data-target="#items">
                        <h4><?= $lang["newsletter_footer_title"] ?></h4>
                    </div>

                    <div class="item-description hidden-xs" id="items">
                        <h4 class="visible-lg"><?= $lang["newsletter_footer_title"] ?></h4>
                        <div id="news-send-info">
                            <p id="get_news"></p>
                            <form class="form-center" id="newsletter-form" method="post" onsubmit="return send_email();">
                            <div class="form-group mx-sm-3 mb-2">
                                <label for="email" class="sr-only"><?= $lang["newsletter_footer_input"] ?></label>
                                <input type="email" class="form-control" id="email_news" placeholder="<?= $lang["newsletter_footer_input"] ?>" required>
                            </div>
                            <button type="submit" onclick="return send_email();" class="btn btn-primary mb-2 button-radius-style"><?= $lang["newsletter_footer_button"] ?></button>
                        </form>
                        </div>
                    </div>
                </div>

                <div class="collapsed-item connect-container">
                    <div class="collapse-element-title visible-xs collapsed" data-toggle="collapse" data-target="#items2">
                        <h4><?= $lang["connect_footer_title"] ?></h4>
                    </div>

                    <div class="item-description hidden-xs" id="items2">
                        <h4 class="visible-lg"><?= $lang["connect_footer_title"] ?></h4>
                        <p class="footer-address">
                            Dinorah de Haro<br />
                            <a href="mailto:dinorah.deharo@solmar.com">dinorah.deharo@solmar.com</a><br />
                            Av. Playa Grande #1 Col. Centro. Cabo San Lucas,<br />
                            B.C.S. Mexico 23450<br />
                            T. +52 624 145 7575 Ext. 74550<br />
                            <a href="mailto:info@solmarfoundation.com">info@solmarfoundation.com</a>
                        </p>
                    </div>
                </div>

                <div class="collapsed-item donate-container">
                    <div class="collapse-element-title visible-xs collapsed" data-toggle="collapse" data-target="#items3">
                        <h4><?= $lang["donate_footer_title"] ?></h4>
                    </div>

                    <div class="item-description hidden-xs" id="items3">
                        <h4 class="visible-lg"><?= $lang["donate_footer_title"] ?></h4>
                        <p>
                            <?= $lang["donate_footer_text"] ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="container second-footer-section">
            <div class="website-links">
                <div>
                    <div class="pages-links">
                        <p>
                            <a href="<?=BASE_PATH?>work"><span><?= $lang["work_menu"] ?></span></a> |
                            <a href="<?=BASE_PATH?>help-us-help"><span><?= $lang["help_menu"] ?></span></a> |
                            <a href="<?=BASE_PATH?>helping-hands"><span><?= $lang["helping_hands_menu"] ?></span></a> |
                            <a href="<?=BASE_PATH?>connect"><span><?= $lang["connect_menu"] ?></span></a>
                        </p>
                    </div>
                    <div class="social-media-links">
                        <p class="social-media">
                            <span><?= $lang["follow_footer_title"] ?></span>
                            <a href="https://www.instagram.com/fundacionsolmar/" target="_blank"><span><i class="fab fa-instagram"></i></span></a>
                            <a href="https://www.facebook.com/SolmarFoundation" target="_blank"><span><i class="fab fa-facebook-f"></i></span></a>
                            <a href="https://twitter.com/FundacionSolmar" target="_blank"><span><i class="fab fa-twitter"></i></span></a>
                            <a href="https://www.pinterest.com.mx/fundacionsolma/" target="_blank"><span><i class="fab fa-pinterest-p"></i></span></a>
                            <a href="https://www.youtube.com/user/SolmarHotelsCabo" target="_blank"><span><i class="fab fa-youtube"></i></span></a>
                        </p>
                    </div>
                    <div class="pages-links">
                        <p>
                            <a href="<?=BASE_PATH?><?= $lang["privacy_notice_link"] ?>" target="_blank"><span><?= $lang["privacy_notice"] ?></span></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="copyright-container">
                <p>Powered by <a href="https://mktideas.com/" target="_blank">MktIdeas</a>.</p>
            </div>
        </section>

    </section>

</footer>
