<!-- Optional JavaScript -->
<script src="<?=BASE_PATH?>static/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $(window).resize(function() {
            if ($(document).width() >= 768) {
                $('.item-description').removeClass('collapse');
            } else {
                $('.item-description').removeClass('show');
                $('.item-description').addClass('collapse');
                $('.collapse-element-title').addClass('collapsed');
            }
        });
        $('.collapse-element-title').click(function() {
            if ($(this).siblings('.item-description').hasClass('show')) {} else {
                $('.item-description').removeClass('show');
                $('.collapse-element-title').addClass('collapsed');
            }
        });
        $(window).scroll(function(event) {
            var scroll = $(window).scrollTop();
            var stateMain = $('main').is("#connect");
            var stateMain2 = $('main').is("#login");
            var stateMain3 = $('main').is('#panel');
            if (scroll > 24) {
                if(!stateMain && !stateMain2 && !stateMain3){
                    $('nav').addClass("fixed-top");
                }
            } else {
                $('nav').removeClass("fixed-top");
            }
        });
        $("#newsletter-form").on('submit', function(e) {
            e.preventDefault();
            swal("<?= $lang['alert_newsletter'] ?>");
        });
    });
</script>
<script>
    function send_email() {
        var em = document.getElementById('email_news').value;
        var dataEm = 'email' + em;
        var url = "<?=BASE_PATH?>services/newsletter.php";
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                document.getElementById('email_news').value = "";
            }
        };
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("email=" + em);

    }
</script>