<div id="social-media-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md">
        <div class="modal-content">
            <div class="row">
                <div class="col-sm-6">
                    <img src="<?=BASE_PATH?>static/img/home/modal-image.jpg" alt="Logo Solmar Foundation">
                </div>
                <div class="col-sm-6">
                    <div class="close-button">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div>
                        <div class="social-media-info">
                            <h1><?= $lang["follow_footer_title"] ?>!</h1>
                            <p><?= $lang["modal_text"] ?></p>
                            <div class="social-media-buttons">
                                <div><a href="https://www.facebook.com/SolmarFoundation" target="_blank">Facebook</a></div>
                                <div><a href="https://twitter.com/FundacionSolmar" target="_blank">Twitter</a></div>
                                <div><a href="https://www.youtube.com/user/SolmarHotelsCabo" target="_blank"><i class="fab fa-youtube"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>