<?php
include "../controllers/config.php";
session_start();
if (isset($_POST["lang"])) {
    $lang = $_POST["lang"];
    if (!empty($lang)) {
        $_SESSION["lang"] = $lang;
    }
}

if (isset($_SESSION["lang"])) {
    $lang = $_SESSION["lang"];
    require "../lang/" . $lang . ".php";
} else {
    require "../lang/en.php";
}

$title = $lang["title_help_us"];
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <?php include '../layouts/head.template.php'; ?>
</head>

<body class="help-us-help">

    <?php include '../layouts/nav.template.php'; ?>

    <main id="help_us_help">
        <div class="principal-container">
            <div class="principal-container">
                <div class="img-border">
                    <div class="principal-image">
                        <img src="<?= BASE_PATH ?>static/img/help_us_help/thanks-for-help.jpg" alt="">
                    </div>
                    <!-- <div class="special-border-bottom">
                    <img src="<?= BASE_PATH ?>static/img/hands-border-1.png" alt="">
                </div> -->
                </div>
                <div class="text-container container text-center">
                    <?= $lang["first_title_help_us"] ?>
                    <div class="five-cols-img">
                        <div>
                            <img src="<?= BASE_PATH ?>static/img/help_us_help/casa-valentina.png" alt="">
                        </div>
                        <div>
                            <img src="<?= BASE_PATH ?>static/img/help_us_help/building-bajas.png" alt="">
                        </div>
                        <div>
                            <img src="<?= BASE_PATH ?>static/img/help_us_help/corazon-de-niño.png" alt="">
                        </div>
                        <div>
                            <img src="<?= BASE_PATH ?>static/img/help_us_help/red-autismo.png" alt="">
                        </div>
                        <div>
                            <img src="<?= BASE_PATH ?>static/img/help_us_help/amigos-de-los-niños.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="green-container">
            <div>
                <div class="special-border-top">
                    <img src="<?= BASE_PATH ?>static/img/border-imgs-top-2.png" alt="">
                </div>
                <div class="special-border-bottom">
                    <img src="<?= BASE_PATH ?>static/img/border-imgs-bottom-2.png" alt="">
                </div>
                <div class="container position-text text-center">
                    <?= $lang["central_text_work"] ?>
                </div>
            </div>
        </div>
        <div class="confirm-donate-container container">
            <div class="text-center">
                <div class="confirm-donation-text">
                    <?= $lang["confirm_donation_help"] ?>
                </div>
                <div class="custom-button">
                    <a href="<?= BASE_PATH ?>donate"><?= $lang["donate_now_home"] ?></a>
                </div>
            </div>
        </div>
    </main>
</body>

<?php include '../layouts/footer.template.php'; ?>

<?php include '../layouts/scripts.template.php'; ?>