<?php
include "../controllers/config.php";
include "../controllers/BlogController.php";

$blogController = new BlogController();

$blogs = $blogController->get('order by id desc');

if (isset($_POST["lang"])) {
	$lang = $_POST["lang"];
	if (!empty($lang)) {
		$_SESSION["lang"] = $lang;
	}
}

if (isset($_SESSION["lang"])) {
	$lang = $_SESSION["lang"];
	require "../lang/" . $lang . ".php";
} else {
	require "../lang/en.php";
}

$title = $lang["title_home"];

?>
<!DOCTYPE html>
<html>

<head>
	<?php include '../layouts/head.template.php'; ?>
</head>

<body class="blogs">
	<?php include '../layouts/nav.template.php'; ?>
	<div class="container mt-5">
		<h1>
			Blogs
		</h1>
		<?php if (isset($_SESSION['usuario'])) : ?>
			<div class="new-blog clearfix">
				<a href="/panel">Añadir Blog</a>
			</div>
		<?php endif ?>
		<div class="row">
			<?php if (isset($blogs) && count($blogs) > 0) : ?>
				<?php foreach ($blogs as $blog) : ?>
					<div class="col-12 col-md-6 col-lg-4 mb-5">
						<div class="card h-100">
							<img src="<?= BASE_PATH ?>static/img/blogs/<?= $blog['imagen_1'] ?>" class="card-img-top" alt="...">
							<div class="card-body">
								<div>
									<h5 class="card-title">
										<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>
									</h5>
									<p>
										<small><?= $blog['fecha'] ?></small>
									</p>
								</div>
								<p class="card-text">
									<?php if ($_SESSION["lang"] == 'es') : ?>

										<?= substr($blog['parrafo_1_es'], 0, 100) ?>...

									<?php else : ?>

										<?= substr($blog['parrafo_1_en'], 0, 100) ?>...

									<?php endif ?>

								</p>
								<a href="<?= BASE_PATH ?>blogs/blog/<?= $blog['slug'] ?>/" class="btn btn-primary">
									<?= $lang["button_show_details_blog"] ?>
								</a>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>
		</div>
	</div>
</body>
<?php include '../layouts/footer.template.php'; ?>

<?php include '../layouts/scripts.template.php'; ?>

</html>