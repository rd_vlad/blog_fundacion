<?php
include "../../controllers/config.php";
include "../../controllers/BlogController.php";

$blogController = new BlogController();

if (!isset($_GET['slug'])) {
	header("Location:" . BASE_PATH);
}
$slug = $_GET['slug'];

$blog = $blogController->show($slug)[0];


if (isset($_POST["lang"])) {
	$lang = $_POST["lang"];
	if (!empty($lang)) {
		$_SESSION["lang"] = $lang;
	}
}

if (isset($_SESSION["lang"])) {
	$lang = $_SESSION["lang"];
	require "../../lang/" . $lang . ".php";
} else {
	require "../../lang/en.php";
}

$title = $lang["title_connect_page"];
?>


<!DOCTYPE html>
<html lang="en">

<head>
	<?php include '../../layouts/head.template.php'; ?>
</head>

<body class="blog">

	<?php include '../../layouts/nav.template.php'; ?>
	<div class="blog">
		<div class="container mb-4 mt-3">
			<div>

				<!-- DISEÑO 1 -->
				<?php if (isset($blog) && $blog['template'] == 1) : ?>

					<?php if ($blog['imagen_1'] != null) : ?>
						<div class="text-center ml-auto mr-auto mb-5">
							<img src="<?= BASE_PATH ?>static/img/blogs/<?= $blog['imagen_1'] ?>" alt="<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>">
						</div>
					<?php endif ?>

					<h2 class="mb-4">
						<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>
					</h2>


					<div class="clearfix mb-3">

						<div class="float-md-left">
							<img src="<?= BASE_PATH ?>static/img/blogs/<?= $blog['imagen_2'] ?>" class="figure-img img-fluid rounded" alt="<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>">
						</div>
						<div>
							<p>
								<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_1_es'] ?><?php else : ?><?= $blog['parrafo_1_en'] ?><?php endif ?></span>
							</p>
						</div>

					</div>

					<div class="clearfix mb-3">
						<div class="float-md-right">
							<div class="img-container">
								<img src="<?= BASE_PATH ?>static/img/blogs/<?= $blog['imagen_3'] ?>" class="figure-img img-fluid rounded" alt="<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>">
							</div>
						</div>
						<div>
							<p>
								<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_2_es'] ?><?php else : ?><?= $blog['parrafo_2_en'] ?><?php endif ?></span>
							</p>
						</div>

					</div>

					<div class="mb-3">
						<p>
							<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_3_es'] ?><?php else : ?><?= $blog['parrafo_3_en'] ?><?php endif ?></span>
						</p>

						<p>
							<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_4_es'] ?><?php else : ?><?= $blog['parrafo_4_en'] ?><?php endif ?></span>
						</p>
					</div>

				<?php endif ?>

				<!-- DISEÑO 2 -->
				<?php if (isset($blog) && $blog['template'] == 2) : ?>

					<?php if ($blog['imagen_1'] != null) : ?>
						<div class="text-center ml-auto mr-auto mb-5">
							<img src="<?= BASE_PATH ?>static/img/blogs/<?= $blog['imagen_1'] ?>" alt="<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>">
						</div>
					<?php endif ?>

					<h2 class="mb-4">
						<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>
					</h2>

					<div class="clearfix mb-3">
						<div class="float-md-right">
							<div class="img-container">
								<img src="<?= BASE_PATH ?>static/img/blogs/<?= $blog['imagen_2'] ?>" alt="<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>">
							</div>
						</div>
						<div>
							<p>
								<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_1_es'] ?><?php else : ?><?= $blog['parrafo_1_en'] ?><?php endif ?></span>
							</p>
						</div>

					</div>

					<div class="clearfix mb-3">

						<div class="float-md-left">
							<img src="<?= BASE_PATH ?>static/img/blogs/<?= $blog['imagen_3'] ?>" class="figure-img img-fluid rounded" alt="<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>">
						</div>
						<div>
							<p>
								<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_2_es'] ?><?php else : ?><?= $blog['parrafo_2_en'] ?><?php endif ?></span>
							</p>
						</div>

					</div>

					<div>
						<p>
							<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_3_es'] ?><?php else : ?><?= $blog['parrafo_3_en'] ?><?php endif ?></span>
						</p>

						<p>
							<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_4_es'] ?><?php else : ?><?= $blog['parrafo_4_en'] ?><?php endif ?></span>
						</p>
					</div>

				<?php endif ?>

				<!-- DISEÑO 3 -->
				<?php if (isset($blog) && $blog['template'] == 3) : ?>

					<?php if ($blog['imagen_1'] != null) : ?>
						<div class="ml-auto mr-auto mb-4">
							<img src="<?= BASE_PATH ?>static/img/blogs/<?= $blog['imagen_1'] ?>" alt="<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>">
						</div>
					<?php endif ?>
					<h2>
						<?= ($_SESSION["lang"] == 'es') ? $blog['titulo_es'] : $blog['titulo_en'] ?>
					</h2>
					<p>
						<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_1_es'] ?><?php else : ?><?= $blog['parrafo_1_en'] ?><?php endif ?></span>
					</p>

					<p>
						<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_2_es'] ?><?php else : ?><?= $blog['parrafo_2_en'] ?><?php endif ?></span>
					</p>

					<p>
						<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_3_es'] ?><?php else : ?><?= $blog['parrafo_3_en'] ?><?php endif ?></span>
					</p>

					<p>
						<span><?php if ($_SESSION["lang"] == 'es') : ?><?= $blog['parrafo_4_es'] ?><?php else : ?><?= $blog['parrafo_4_en'] ?><?php endif ?></span>
					</p>

				<?php endif ?>

			</div>
		</div>
	</div>
</body>

<?php include '../../layouts/footer.template.php'; ?>

<?php include '../../layouts/scripts.template.php'; ?>