<?php
include "../controllers/config.php";
session_start();
if (isset($_POST["lang"])) {
  $lang = $_POST["lang"];
  if (!empty($lang)) {
    $_SESSION["lang"] = $lang;
  }
}

if (isset($_SESSION["lang"])) {
  $lang = $_SESSION["lang"];
  require "../lang/" . $lang . ".php";
} else {
  require "../lang/en.php";
}

$title = $lang["title_donate"];
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../layouts/head.template.php'; ?>
</head>

<body class="donate">

<?php include '../layouts/nav.template.php'; ?>

<main id="donate">
    <div class="principal-container">
        <div class="img-border">
            <div class="principal-image">
                <img src="<?=BASE_PATH?>static/img/donate/hug-guys.jpg" alt="">
            </div>
            <div class="special-border-bottom">
                <img src="<?=BASE_PATH?>static/img/border-imgs-bottom-3.png" alt="">
            </div>
        </div>
        <div class="text-container container text-center">
            <?=$lang["info_donate"]?>
            <div class="custom-button">
                <a href="https://donate.icfdn.org/npo/solmar-foundation-fund" target="_blank"><?=$lang["donate_now_home"]?><span>International Community Foundation</span></a>
            </div>
        </div>
    </div>
</main>
</body>

<?php include '../layouts/footer.template.php'; ?>

<?php include '../layouts/scripts.template.php'; ?>