<?php
include "../controllers/config.php";
session_start();
if (isset($_POST["lang"])) {
    $lang = $_POST["lang"];
    if (!empty($lang)) {
        $_SESSION["lang"] = $lang;
    }
}

if (isset($_SESSION["lang"])) {
    $lang = $_SESSION["lang"];
    require "../lang/" . $lang . ".php";
} else {
    require "../lang/en.php";
}

$title = $lang["title_work"];
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <?php include '../layouts/head.template.php'; ?>
</head>

<body class="work">

    <?php include '../layouts/nav.template.php'; ?>

    <main id="work">
        <div class="principal-container">
            <div class="img-border">
                <div class="principal-image">
                    <img src="<?=BASE_PATH?>static/img/work/guys-together.jpg" alt="">
                </div>
                <!-- 
            <div class="special-border-bottom">
                <img src="<?=BASE_PATH?>static/img/border-imgs-bottom-3.png" alt="">
            </div> -->
            </div>
            <div class="text-container container text-center">
                <?= $lang["first_title_work"] ?>
            </div>
        </div>
        <div class="kindness-container special-text-img parallax-effect">
            <div class="over-title">
                <img class="left-quote" src="<?=BASE_PATH?>static/img/quote-char-red.png" alt="">
                <img class="right-quote" src="<?=BASE_PATH?>static/img/quote-char-red.png" alt="">
                <h1>
                    <?= $lang["kindness_title_work"] ?>
                </h1>
            </div>
        </div>
        <div class="confirm-donate-container container">
            <div class="text-center">
                <div class="confirm-donation-text">
                    <h1>
                        <?= $lang["confirm_donation_work"] ?>
                    </h1>
                </div>
                <div class="custom-button">
                    <a href="<?=BASE_PATH?>donate"><?= $lang["donate_now_home"] ?></a>
                </div>
            </div>
        </div>
    </main>
</body>

<?php include '../layouts/footer.template.php'; ?>

<?php include '../layouts/scripts.template.php'; ?>