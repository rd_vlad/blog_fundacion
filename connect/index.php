<?php
include "../controllers/config.php";
session_start();
if (isset($_POST["lang"])) {
    $lang = $_POST["lang"];
    if (!empty($lang)) {
        $_SESSION["lang"] = $lang;
    }
}

if (isset($_SESSION["lang"])) {
    $lang = $_SESSION["lang"];
    require "../lang/" . $lang . ".php";
} else {
    require "../lang/en.php";
}

$title = $lang["title_connect_page"];
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <?php include '../layouts/head.template.php'; ?>
</head>

<body class="connect">

    <?php include '../layouts/nav.template.php'; ?>
    <main id="connect">
        <div class="contact-container container">
            <div>
                <div class="contact-form">
                    <form action="<?=BASE_PATH?>/services/send.php" method="post" class="form-contact">
                        <div class="form-group">
                            <label><?= $lang["name_form_connect"] ?>: <span>*</span></label>
                            <input type="text" name="name" placeholder="<?= $lang["name_form_connect"] ?>" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label><?= $lang["email_form_connect"] ?>: <span>*</span></label>
                            <input type="email" name="email" placeholder="<?= $lang["email_form_connect"] ?>" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label><?= $lang["phone_form_connect"] ?>:</label>
                            <input type="tel" name="phone" placeholder="<?= $lang["phone_form_connect"] ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label><?= $lang["message_form_connect"] ?>:</label>
                            <textarea name="message" class="form-control"></textarea>
                        </div>

                        <input type="submit" value="<?= $lang["submit_form_connect"] ?>" class="btn btn-form">
                    </form>
                </div>
                <div class="contact-info">
                    <h1><?= $lang["title_connect"] ?></h1>
                    <p>
                        Dinorah de Haro<br>
                        <a href="mailto:dinorah.deharo@solmar.com">dinorah.deharo@solmar.com</a>
                    </p>
                    <p>
                        Av. Playa Grande #1 Col. Centro. Cabo San Lucas,<br>
                        B.C.S. Mexico 23450<br>
                        T. +52 624 145 7575 Ext. 74550<br>
                        <a href="mailto:info@solmarfoundation.com">info@solmarfoundation.com</a>
                    </p>
                    <div class="confirm-donate-container">
                        <div>
                            <div class="confirm-donation-text">
                                <p>
                                    <?= $lang["confirm_donation_connect"] ?>
                                </p>
                            </div>
                            <div class="custom-button">
                                <a href="<?=BASE_PATH?>donate"><?= $lang["donate_now_home"] ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>

<?php include '../layouts/footer.template.php'; ?>

<?php include '../layouts/scripts.template.php'; ?>