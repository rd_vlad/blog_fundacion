<?php
include "controllers/config.php";
if (isset($_POST["lang"])) {
    $lang = $_POST["lang"];
    if (!empty($lang)) {
        $_SESSION["lang"] = $lang;
        header('Location: home');
    }
}

if (isset($_SESSION["lang"])) {
    $lang = $_SESSION["lang"];
    require "lang/" . $lang . ".php";
} else {
    require "lang/en.php";
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="euc-jp">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="We work together to create a better tomorrow. Your donations allow Solmar Foundation to work hand-in-hand with several non-government agencies including orphanages, senior centers and shelters in Baja California Sur." />
    <meta name="Keywords" content="solmar foundation,solmar,donations,ong,Foundation" />
    <meta name=��distribution�� content=��global�� />
    <title>Solmar Foundation</title>
    <link rel="stylesheet" href="static/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/css/style.css">
</head>

<body id="principal_page">
    <section class="main">
        <div class="position-center">
            <div class="img-container"><img src="static/img/logos/solmar_fundation_principal.png" alt="Logo Solmar Foundation"></div>
            <form method="POST">
                <div class="buttons_languaje">
                    <a href="<?= BASE_PATH ?>home"><button name="lang" value="en" type="submit">English</button></a>
                    <a href="<?= BASE_PATH ?>home"><button name="lang" value="es" type="submit">Español</button></a>
                </div>
            </form>
        </div>
        <div class="video-container">
            <video autoplay loop muted>
                <source src="static/video/solmar_foundation.mp4" type="video/mp4">
            </video>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
</body>

</html>