<?php
include "../controllers/config.php";
session_start();
if (isset($_POST["lang"])) {
  $lang = $_POST["lang"];
  if (!empty($lang)) {
    $_SESSION["lang"] = $lang;
  }
}

if (isset($_SESSION["lang"])) {
  $lang = $_SESSION["lang"];
  require "../lang/" . $lang . ".php";
} else {
  require "../lang/en.php";
}

$title = $lang["title_confirm_message"];
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../layouts/head.template.php'; ?>
</head>

<body class="message-sent">

<?php include '../layouts/nav.template.php'; ?>

<main id="message_sent">
    <div class="confirmation-message-container container text-center">
        <?=$lang["confirm_message"]?>
    </div>
</main>
</body>

<?php include '../layouts/footer.template.php'; ?>

<?php include '../layouts/scripts.template.php'; ?>