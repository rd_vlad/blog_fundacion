<?php include 'basics/header.php' ?>
<script>
    $(document).ready(function() {
        document.title = '<?=$lang["title_confirm_message"]?>';
    });
</script>
<main id="message_sent">
    <div class="confirmation-message-container container text-center">
        <?=$lang["confirm_message"]?>
    </div>
</main>
<?php include 'basics/footer.php' ?>