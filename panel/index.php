<?php

include "../controllers/BlogController.php";
include "../controllers/AuthController.php";

if (empty($_SESSION['usuario'])) {
    header('location:../login/index.php');
}

$blogController = new BlogController();

$blogs = $blogController->get('');
#echo $_SESSION['token'];

if (isset($_POST["lang"])) {
    $lang = $_POST["lang"];
    if (!empty($lang)) {
        $_SESSION["lang"] = $lang;
    }
}

if (isset($_SESSION["lang"])) {
    $lang = $_SESSION["lang"];
    require "../lang/" . $lang . ".php";
} else {
    require "../lang/en.php";
}

$title = $lang["title_home"];
?>
<!DOCTYPE html>
<html>

<head>
    <?php include '../layouts/head.template.php'; ?>
    <meta name="robots" content="index,nofollow">
</head>

<body class="panel">
    <?php include '../layouts/nav.template.php'; ?>
    <div id="wrapper">

        <div class="container-fluid mt-3">

            <!-- NOTIFICATION -->
            <?php if (isset($_SESSION['status']) && $_SESSION['status'] == "success") : ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Correcto!</strong> <?= $_SESSION['message'] ?>.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php unset($_SESSION['status']); ?>
            <?php endif ?>

            <?php if (isset($_SESSION['status']) && $_SESSION['status'] == "error") : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Error!</strong> <?= $_SESSION['message'] ?>.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <?php unset($_SESSION['status']); ?>
            <?php endif ?>

            <!-- CARD Y TABLE -->
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-header">
                            Lista de blogs registrados

                            <button type="button" data-toggle="modal" data-target="#staticBackdrop" class="btn btn-primary float-right" onclick="add()">
                                Añadir blog
                            </button>
                        </div>
                        <div class="card-body">

                            <table class="table table-bordered table-striped">
                                <thead class="thead-dark ">
                                    <tr>
                                        <th scope="col">Fecha</th>
                                        <th scope="col">Título</th>
                                        <th scope="col">Slug</th>
                                        <th scope="col">Párrafo 1</th>
                                        <th scope="col">Párrafo 2</th>
                                        <th scope="col">Párrafo 3</th>
                                        <th scope="col">Párrafo 4</th>
                                        <th scope="col">Imagen 1</th>
                                        <th scope="col">Imagen 2</th>
                                        <th scope="col">Imagen 3</th>
                                        <th scope="col">Template</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (isset($blogs) && count($blogs) > 0) : ?>
                                        <?php foreach ($blogs as $blog) : ?>

                                            <tr>
                                                <th scope="row">
                                                    <?= $blog['fecha'] ?>
                                                </th>
                                                <td>
                                                    <?= $blog['titulo_es'] ?>
                                                </td>
                                                <td>
                                                    <?= $blog['slug'] ?>
                                                </td>
                                                <td>
                                                    <?= substr($blog['parrafo_1_es'], 0, 50) ?>...
                                                </td>
                                                <td>
                                                    <?= substr($blog['parrafo_2_es'], 0, 50) ?>...
                                                </td>
                                                <td>
                                                    <?= substr($blog['parrafo_3_es'], 0, 50) ?>...
                                                </td>
                                                <td>
                                                    <?= substr($blog['parrafo_4_es'], 0, 50) ?>...
                                                </td>
                                                <td>
                                                    <?= $blog['imagen_1'] ?>
                                                </td>
                                                <td>
                                                    <?= $blog['imagen_2'] ?>
                                                </td>
                                                <td>
                                                    <?= $blog['imagen_3'] ?>
                                                </td>
                                                <td>
                                                    <?= $blog['template'] ?>
                                                </td>
                                                <td>
                                                    <button data-info='<?= json_encode($blog) ?>' data-toggle="modal" data-target="#staticBackdrop" type="button" class="btn btn-warning btn-sm btn-block" onclick="editar(this)">
                                                        <i class="fa fa-pencil"></i> Editar
                                                    </button>
                                                    <button onclick="remove(<?= $blog['id'] ?>,this)" type="button" class="btn btn-danger btn-sm btn-block">
                                                        <i class="fa fa-trash"></i> Eliminar
                                                    </button>
                                                </td>
                                            </tr>

                                        <?php endforeach ?>
                                    <?php endif ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">
                        Agregar blog
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form method="POST" id="myForm" action="../controllers/BlogController.php" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="template">
                                        Plantilla
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fas fa-exchange-alt"></i>
                                            </span>
                                        </div>
                                        <select class="form-control" name="template" id="template" onchange="cambioTemplate($(this).val())">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <p class="mg-b-10">
                                        <i class="fas fa-image"></i>
                                        Imagen de Cabezera
                                    </p>
                                    <input type="file" name="imagen_1" id="imagen_1" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6 text-center">
                                Español
                                <hr>
                            </div>

                            <div class="col-md-6 text-center">
                                Inglés
                                <hr>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="titulo_es">
                                        Titulo
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-heading"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control " id="titulo_es" aria-describedby="emailHelp" placeholder="Título" required="" name="titulo_es">
                                    </div>
                                    <!-- <small id="emailHelp" class="form-text text-muted">
                                    No ingresar números.
                                </small> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="titulo_en">
                                        Title
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-heading"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control " id="titulo_en" aria-describedby="emailHelp" placeholder="Title" required="" name="titulo_en">
                                    </div>
                                    <!-- <small id="emailHelp" class="form-text text-muted">
                                    No ingresar números.
                                </small> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="parrafo_1_es">
                                        Parrafo 1
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-file-alt"></i>
                                            </span>
                                        </div>
                                        <textarea class="form-control" id="parrafo_1_es" required="" name="parrafo_1_es" placeholder="" cols="30" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="parrafo_1_en">
                                        Paragraph 1
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-file-alt"></i>
                                            </span>
                                        </div>
                                        <textarea class="form-control" id="parrafo_1_en" required="" name="parrafo_1_en" placeholder="" cols="30" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="parrafo_2_es">
                                        Parrafo 2
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-file-alt"></i>
                                            </span>
                                        </div>
                                        <textarea class="form-control" id="parrafo_2_es" required="" name="parrafo_2_es" placeholder="" cols="30" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="parrafo_2_en">
                                        Paragraph 2
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-file-alt"></i>
                                            </span>
                                        </div>
                                        <textarea class="form-control" id="parrafo_2_en" required="" name="parrafo_2_en" placeholder="" cols="30" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="parrafo_3_es">
                                        Parrafo 3
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-file-alt"></i>
                                            </span>
                                        </div>
                                        <textarea class="form-control" id="parrafo_3_es" name="parrafo_3_es" placeholder="" cols="30" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="parrafo_1_en">
                                        Paragraph 3
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-file-alt"></i>
                                            </span>
                                        </div>
                                        <textarea class="form-control" id="parrafo_3_en" name="parrafo_3_en" placeholder="" cols="30" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="parrafo_4_es">
                                        paragraph 4
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-file-alt"></i>
                                            </span>
                                        </div>
                                        <textarea class="form-control" id="parrafo_4_es" name="parrafo_4_es" placeholder="" cols="30" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="parrafo_1_en">
                                        Paragraph 4
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fa fa-file-alt"></i>
                                            </span>
                                        </div>
                                        <textarea class="form-control" id="parrafo_4_en" name="parrafo_4_en" placeholder="" cols="30" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <p class="mg-b-10">
                                        <i class="fas fa-image"></i>
                                        Imagen contenido 1
                                    </p>
                                    <input type="file" name="imagen_2" id="imagen_2" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <p class="mg-b-10">
                                        <i class="fas fa-image"></i>
                                        Imagen contenido 2
                                    </p>
                                    <input type="file" name="imagen_3" id="imagen_3" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="template">
                                        <small>URL amigable del blog</small>
                                    </label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="fas fa-globe"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control " id="slug" aria-describedby="emailHelp" placeholder="nombre-de-blog" required="" name="slug">
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Cancelar
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Guardar
                        </button>
                        <input type="hidden" id="action_form" name="action" value="store">
                        <input type="hidden" name="id" id="id">
                        <input type="hidden" name="token" value="<?= $_SESSION['token'] ?>">
                    </div>
                </form>

            </div>

        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script type="text/javascript">
        function cambioTemplate(temp) {
            if (temp != 3) {
                $("#imagen_1").parent().parent().show();
                $("#imagen_2").parent().parent().show();
                $("#imagen_3").parent().parent().show();
            } else {
                $("#imagen_2").parent().parent().hide();
                $("#imagen_3").parent().parent().hide();
            }
        }

        function remove(id, target) {
            swal({
                    title: "",
                    text: "¿Desea eliminar el usuario?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    buttons: ["Cancelar", "Eliminar"]
                })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajax({
                            url: '../controllers/BlogController.php',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                action: 'remove',
                                blog_id: id,
                                token: '<?= $_SESSION['token'] ?>'
                            },
                            success: function(json) {
                                console.log(json)

                                if (json.status == 'success') {
                                    swal(json.message, {
                                        icon: "success",
                                    });
                                    $(target).parent().parent().remove();
                                } else {
                                    swal(json.message, {
                                        icon: "error",
                                    });
                                }

                            },
                            error: function(xhr, status) {
                                console.log(xhr)
                                console.log(status)
                            }
                        })



                    } else {}
                });
        }

        function editar(target) {

            var info = $(target).data('info');

            $("#staticBackdropLabel").text('Editar blog')

            $("#titulo_es").val(info.titulo_es)
            $("#titulo_en").val(info.titulo_en)
            $("#template").val(info.template)
            $("#slug").val(info.slug)
            $("#parrafo_1_es").val(info.parrafo_1_es)
            $("#parrafo_2_es").val(info.parrafo_2_es)
            $("#parrafo_3_es").val(info.parrafo_3_es)
            $("#parrafo_4_es").val(info.parrafo_4_es)
            $("#parrafo_1_en").val(info.parrafo_1_en)
            $("#parrafo_2_en").val(info.parrafo_2_en)
            $("#parrafo_3_en").val(info.parrafo_3_en)
            $("#parrafo_4_en").val(info.parrafo_4_en)
            $("#id").val(info.id)
            $("#action_form").val('update')
            cambioTemplate(info.template) 
        }

        function add() {
            $("#staticBackdropLabel").text('Agregar blog')
            $("#action_form").val('store')
            document.getElementById("myForm").reset();
            cambioTemplate($("#template").val())
        }
    </script>
</body>

</html>