<?php

$lang = array(
    //general/header
    "alert_newsletter" => "Su correo ha sido registrado. Gracias!",
    "alert_newsletter_error" => "Su correo no pude ser registrado, intente de nuevo por favor.",
    "title_home" => "Home - Fundación Solmar",
    "title_work" => "Trabajamos Juntos - Fundación Solmar",
    "title_help_us" => "Ayudanos a Ayudar - Fundación Solmar",
    "title_helping" => "Helping Hands - Fundación Solmar",
    "title_donate" => "Donar Ahora! - Fundación Solmar",
    "title_connect_page" => "Contáctanos! - Fundación Solmar",
    "title_error" => "Error 404 - Fundación Solmar",
    "title_confirm_message" => "Mensaje Enviado - Fundación Solmar",
    "languaje" => "Español",
    "languaje_translade" => "English",
    "languaje_translade_abr" => "en",
    "title" => "Fundación Solmar",
    "description" => "Weas Multilenguaje",
    "work_menu" => "Nosotros",
    "help_menu" => "Ayudanos a Ayudar",
    "helping_hands_menu" => "Helping Hands",
    "connect_menu" => "Contacto",
    "donate_menu" => "Donar",
    "blogs_menu" => "Blogs",
    "view_more" => "Ver Más",
    "logout_menu" => "CERRAR SESIÓN",
    //footer
    "newsletter_footer_title" => "Recibe Nuesto Boletín",
    "connect_footer_title" => "Contáctanos",
    "donate_footer_title" => "Donación",
    "newsletter_footer_input" => "Correo",
    "newsletter_footer_button" => "Suscribete",
    "donate_footer_text" => "Confirme su donación al momento del check-out.<br />Haga una donación en línea deducible de impuestos.<br /><br />Llámenos desde su suite. Marque la extensión 74540<br />Llámenos desde el interior de la republica. Marque (624) 145-7540<br />Llámenos desde los EE. UU. Marque (310) 209-8430",
    "follow_footer_title" => "Siguenos",
    "privacy_notice" => "Aviso de privacidad",
    "privacy_notice_link" => "static/pdf/Aviso-de-privacidad.pdf",

    //Home
    "modal_text" => "Conoce a la gente que ayudas con tu donación.",
    "first_title_home" => "Una fundación arraigada en la historia de Los Cabos",
    "about_text_home" => "<h1>Fundación Solmar</h1>
    <p>Los objetivos principales de la Fundación Solmar se explican mejor a través de la historia del Grupo Solmar y su fundador, Don Luis Bulnes. Cuando Don Luis Bulnes llegó a Los Cabos, el pueblo era poco más que un pequeño pueblo de pescadores con una pista de aterrizaje y una fábrica de conservas de pescado. Su enfoque visionario creó infraestructuras de agua, electricidad y conectividad que transformaron Los Cabos en el bullicioso y exitoso destino turístico que es hoy.</p>
    <p> La salud y la prosperidad de Los Cabos están profundamente entrelazadas con la visión del Grupo Solmar. En línea con este punto de vista, nuestra compañía creó la Fundación Solmar, una organización sin fines de lucro que ayuda a los miembros de la comunidad que más lo necesitan. Debido al papel de nuestro fundador como Presidente del Patronato del Departamento de Bomberos, nos enorgullece decir que la Fundación Solmar recibió el elogio presidencial de México en nuestros esfuerzos por brindar ayuda a los menos afortunados.</p>",
    "donation_become_smiles_home" => "Tus donaciones se convierten en sonrisas",
    "commitment_title_home" => "Compromiso de ayudar",
    "commitment_text_home" => "<p>Nos honra tener la oportunidad de ayudar a los necesitados en toda nuestra comunidad con la ayuda de nuestros valiosos socios de tiempo compartido y huespedes de hoteles Solmar.</p>
    <p>Cuando realiza una donación a la Fundación Solmar, el 100% de su donación se destina a obras de caridad, el desarrollador iguala su donativo dólar por dólar, y todos los gastos administrativos de la Fundación Solmar son cubiertos por el desarrollador.</p>
    <p><b>Fundación Solmar</b> trabaja con otras agencias públicas y organizaciones no gubernamentales para combinar recursos y asistencia directa a las áreas donde más se necesita.</p>",
    "purpose_home_link" => "Propósito",
    "little_help_home_link" => "<span class='first-line'>Un poco de</span><span class='second-line'>ayuda,</span><span class='third-line'>ayuda mucho</span>",
    "help_us_home_link" => "Ayudanos a Ayudar",
    "work_home_link" => "<span class='second-line'>Equipo</span>",
    "mission_home" => "Nuestra misión es dignificar a los más necesitados de nuestra comunidad y ayudarlos a lograr una vida mejor a través de programas comunitarios y las actividades de la Fundación Solmar.",
    "helping_hands_home_link" => "<span class='first-line'>Helping</span><span class='second-line'>Hands</span>",
    "spare_title_home" => "Tu Cambio Puede Salvar Vidas<span>En promedio, la Fundación Solmar:</span>",
    "actions_home" => "<div class='four-cols-text'><div><p><span>Donación de </span><span>$174,000 USD </span><span>por año a</span><span>varias ONG</span></p></div><div><p><span>Ayuda a <span>23</span> </span><span>asociaciones</span></p></div><div><p><span>Distribución de </span><span>3,000 canastas </span><span>de alimentos por año</span></p></div><div><p><span>Organizamos </span><span>12 talleres de </span><span>habilidades </span><span> laborales por año</span></p></div></div>
    <div class='two-cols-text'><div><p><span>Realizamos donaciones mensuales para </span><span>complementar los ingresos de 2,000 familias locales.</span></p></div><div><p><span>Brindamos 245 becas para que los niños </span><span>continuen con sus estudios.</span></p></div></div>",
    "our_work_home1" => "2017 Honoris Causa de UGC para la mejor fundación en Baja California Sur.",
    "our_work_home2" => "Premio de la Paz de My World UN Monterrey por los programas para generar empleos para personas con discapacidad intelectual.",
    "our_work_home3" => "Mención de Honor CEPSA IAP para directora de fundaciones donantes.",
    "our_work_home4" => "Premio a la empresa inclusiva del Ministerio de Trabajo de México por programas para crear empleos.",
    "our_work_home5" => "Hotel del año 2017 de Community Hotels para el trabajo comunitario de la Fundación - Premio Palma de Oro.",
    "confirm_donation_home" => "Confirme su donación al momento del Check-Out. <span>Haga una donación en línea deducible de impuestos.</span>",
    "donate_now_home" => "Donar Ahora!",
    //work
    "first_title_work" => "<h1>Trabajamos juntos<span>Para Crear un Mejor Mañana</span></h1>
    <p class='principal-text'>Sus donativos le permiten a la Fundación Solmar trabajar de la mano con varias agencias no gubernamentales, incluidos orfanatos, centros para personas mayores y refugios en Baja California Sur.</p>
    <p class='second-text'>Como una fundación de segundo nivel, la Fundación Solmar puede combinar recursos y asistencia directa a las áreas donde más se necesitan.<br>Con su apoyo, la Fundación Solmar también ejecuta programas específicos para ayudar a personas con discapacidades de aprendizaje, mujeres en situaciones vulnerables, niños desfavorecidos y más.</p>",
    "kindness_title_work" => "La amabilidad es un regalo que trae alegría a muchos",
    "confirm_donation_work" => "Confirme su Donación al Momento del Check-Out.<span>Haga una donación en linea deducible de impuestos.</span>",
    //help_us_help
    "first_title_help_us" => "<h1>Ayudanos a Ayudar</h1><p class='principal-text'>Su apoyo hace posible que la Fundación Solmar ayude al trabajo de varias asociaciones sin fines de lucro como:</p>",
    "central_text_work" => "<p>Durante su estadía en este Resort de Solmar, puede hacer una donación voluntaria que se agregará a su factura. <span>Hay un cargo opcional de $ 10USD por semana. ¡Esa pequeña donación es muy útil!</span></p><p>Si desea convertirse en patrocinador de uno de nuestros programas, puede hacer una donación deducible de impuestos en línea para que también pueda ayudar a mejorar la vida de la gente de Los Cabos donde quiera que estés!.</p>",
    "confirm_donation_help" => "<h1>Confirme su Donación al Momento del Check-Out.</h1>",
    //helping_hands
    "first_title_helping" => "<h1>Únete al recorrido de Helping Hands</h1><p><span>Puedes Conocer a las Personas</span><span>a las Que Estás Ayudando Con Tu Donación</span></p>
    <div class='two-cols-text'>
    <div>
        <p class='text-justify'>
        En este recorrido de dos horas, verá de primera mano el impacto significativo que su donación tiene en las familias.
        </p>
        <p class='text-justify'>
        Regístrese hoy en el mostrador de concierge y únase a nosotros en el recorrido gratuito que sale todos los miércoles a las 9 am desde el lobby de Playa Grande Resort & Grand Spa. El recorrido es gratuito y el espacio es limitado. Consultar disponibilidad durante temporada de lluvias y feriados.
        </p>
        <p>
        Experimente la vida de un voluntario y vea cómo sus donaciones impactan la vida de muchos para mejor.
        </p>
    </div>
    <div>
        <div class='yellow-container'>
            <h1>
                Empaque con Propósito
            </h1>
            <p>
                Mientras prepara su próximo viaje a Los Cabos, recuerde que hay miles de familias que necesitan su ayuda.<br>Uno de nuestros programas ayuda a los niños a permanecer en la escuela. Al incluir algunos útiles escolares en su equipaje, como cuadernos, lápices, bolígrafos, mochilas, estuches, sacapuntas, borradores y reglas, puede ayudar a crear un mejor ambiente de aprendizaje y alentar a los niños a continuar su educación.
            </p>
        </div>
    </div>
</div>
<div class='final-paragraph'>
    <p>
        Pase por la oficina de la Fundación Solmar para más detalles.
    </p>
</div>",
"transform_world_helping" => "Ayuda que transforma <span>su mundo</span>",
"confirm_donate_helping" => "Un regalo de ropa usada es siempre motivo de celebración. Algo que siempre se necesita es ropa interior. La ropa interior nueva para mujeres y niños es fácil de empacar y hay muchas familias que le agradecerán su amabilidad.",
"contact_us_connect" => "Contáctanos",
"name_form_connect" => "Nombre",
"email_form_connect" => "Correo",
"phone_form_connect" => "Telefono",
"message_form_connect" => "Mensaje",
"submit_form_connect" => "Envía Tu Mensaje",
"title_connect" => "Contacto",
"confirm_donation_connect" => "Confirma Tu Donación al Momento<span>De Hacer Tu Check-Out.</span>",
"error_404" => "Página No Encontrada",
"confirm_message" => "<h1>Tu mensaje fue enviado!</h1><h2>Nos pondremos en contacto contigo pronto</h2><p>Si quieres enviar otro mensaje, da click <a href='/connect'>aquí</a></p>",
//donate
"info_donate" => "<div>
                <h1>Transferencia Bancaria</h1>
                <p>
                    Banco: Banco Santander México SA<br>
                    Nombre de la cuenta: Fundación Solmar AC
                </p>
                <p>
                    <span>Pesos Mexicanos (MN)</span>
                    Número de cuenta: 65502980417<br>
                    Clabe: 014041655029804179
                </p>
                <p>
                    <span>Dólares Estadounidenses (USD)</span>
                    Número de cuenta: 82500539844<br>
                    Clabe: 014041825005398442
                </p>
            </div>
            <div>
                <h1>Dona a través de cheque</h1>
                <p>
                    Hacer cheque a nombre de: Fundación Solmar AC<br>
                    Enviar cheque en México a:
                </p>
                <p>
                    C.P. Haydee Olvera Minjarez<br>
                    Depto Vicepresidencia<br>
                    Av. Solmar no. 1, interior Hotel Playa Grande, Colonia Centro, C.P. 23450<br>
                    Cabo San Lucas, Baja California Sur.
                </p>
            </div>
            <div>
                <h1>Donar en línea</h1>
                <p>Haz click en el botón de abajo para donar a Fundación Solmar en dólares americanos, usando una tarjeta de crédito o débito a través de ICF.</p>
            </div>",
    "button_show_details_blog" => "Leer más",
);