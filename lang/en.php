<?php

$lang = array(
    //general/header
    "alert_newsletter" => "Your email has been registered. Thank you!",
    "alert_newsletter_error" => "Your email can not be registered. Try again please",
    "title_home" => "Home - Solmar Foundation",
    "title_work" => "Work - Solmar Foundation",
    "title_help_us" => "Help Us Help - Solmar Foundation",
    "title_helping" => "Helping Hands - Solmar Foundation",
    "title_donate" => "Donate Now! - Solmar Foundation",
    "title_connect_page" => "Contact Us! - Solmar Foundation",
    "title_error" => "404 Error - Solmar Foundation",
    "title_confirm_message" => "Message Sent - Solmar Foundation",
    "languaje" => "English",
    "languaje_translade" => "Español",
    "languaje_translade_abr" => "es",
    "title" => "Solmar Foundation",
    "description" => "Multilanguaje things",
    "work_menu" => "Work",
    "help_menu" => "Help Us Help",
    "helping_hands_menu" => "Helping Hands",
    "connect_menu" => "Contact",
    "donate_menu" => "Donate!",
    "blogs_menu" => "Blogs",
    "view_more" => "View More",
    "logout_menu" => "LOG OUT",
    //footer
    "newsletter_footer_title" => "Get Our Newsletter",
    "connect_footer_title" => "Contact",
    "donate_footer_title" => "Donate",
    "newsletter_footer_input" => "Email Adress",
    "newsletter_footer_button" => "Subscribe",
    "donate_footer_text" => "Confirm your donation at check-out.<br />Make a tax-deductible online donation.<br /><br />Call us from your suite. Dial extension 74540<br />Call us from inside Mexico. Dial (624) 145-7540<br />Call us from the US. Dial (310) 209-8430",
    "follow_footer_title" => "Follow Us",
    "privacy_notice" => "Privacy Notice",
    "privacy_notice_link" => "static/pdf/Aviso-de-privacidad-ing.pdf",
    //Home
    "modal_text" => "You can meet the people you are helping with your donation",
    "first_title_home" => "A Foundation Rooted in the History of Los Cabos",
    "about_text_home" => "<h1>Solmar Foundation</h1>
    <p>The primary goals of the Solmar Foundation are best explained through the history of the Solmar Group and its founder, Don Luis Bulnes. When Don Luis Bulnes came to Los Cabos, the town was little more than a small fishing village with a landing strip and a fish cannery. His visionary approach created water, electricity, and connectivity infrastructures that transformed Los Cabos into the bustling and successful tourist destination it is today</p>
    <p> The health and prosperity of Los Cabos is deeply intertwined with the vision of the Solmar Group. Aligned with this view, our company created the Solmar Foundation, a non-profit organization which lends a helping hand to members of the community that need it most. Because of our founder’s role as President of the Fire Department’s Patronage, we are proud to say the Solmar Foundation received Mexico’s Presidential Commendation in our efforts to provide aid to the less fortunate. </p>",
    "donation_become_smiles_home" => "Your donations become smiles",
    "commitment_title_home" => "Commitment to Help",
    "commitment_text_home" => "<p>We are honored to have the opportunity to assist those in need throughout our community with the help of our valued guests and members.</p>
    <p>When you donate to Solmar Foundation, 100% of your donation goes toward charity, the developer matches your donation dollar per dollar, and all the administrative expenses of Solmar Foundation are covered by the developer.</p>
    <p><b>Solmar Foundation</b> works with other public agencies and non-government organizations to combine resources and direct assistance to the areas where it is most needed.</p>",
    "purpose_home_link" => "Purpose",
    "little_help_home_link" => "<span class='first-line'>A little</span><span class='second-line'>help</span><span class='third-line'>goes a long way</span>",
    "help_us_home_link" => "Help Us Help",
    "work_home_link" => "<span class='first-line'>Work</span><span class='second-line'>Together</span>",
    "mission_home_link" => "",
    "mission_home" => "Our mission is to dignify the neediest members in our community and help them achieve a better life through community programs and the activities of Solmar Foundation.",
    "helping_hands_home_link" => "<span class='first-line'>Helping</span><span class='second-line'>Hands</span>",
    "spare_title_home" => "Your Spare Change Can Change Lives<span>On average, the Solmar Foundation:</span>",
    "actions_home" => "<div class='four-cols-text'><div><p><span>Donate </span><span>$174,000 USD </span><span>per year to</span><span>various NGO’s</span></p></div><div><p><span>Helps <span>23</span> </span><span>associations</span></p></div><div><p><span>Distributes </span><span>3,000 food </span><span>baskets per year</span></p></div><div><p><span>Organizes </span><span>12 job-skills </span><span>workshops </span><span>per year</span></p></div></div>
    <div class='two-cols-text'><div><p><span>Makes monthly donations to supplement </span><span>the income of 2,000 local families</span></p></div><div><p><span>Provides 245 scholarships for children </span><span>to continue their studies</span></p></div></div>",
    "our_work_home1" => "2017 Honoris Causa from UGC for the best foundation in Baja California Sur",
    "our_work_home2" => "Peace Award from My World UN Monterrey for the programs to generate jobs for people with intellectual disabilities",
    "our_work_home3" => "CEPSA IAP Honor Mention for female director from donating foundations",
    "our_work_home4" => "Inclusive Company Award from Mexico’s Ministry of Labor for programs to create jobs",
    "our_work_home5" => "2017 Hotel of the Year by Community Hotels for the community work of the Foundation",
    "confirm_donation_home" => "Confirm Your Donation At Check-Out. <span>Make a tax-deductible online donation.</span>",
    "donate_now_home" => "Donate Now!",
    //work
    "first_title_work" => "<h1>We Work Together<span>To Create a Better Tomorrow</span></h1><p class='principal-text'>Your donations allow Solmar Foundation to work hand-in-hand with several non-government agencies including orphanages, senior centers and shelters in Baja California Sur.</p><p class='second-text'><b>As a second- tier foundation</b>, the Solmar Foundation is able to combine resources and direct <b>assistance to the areas where it is they are most needed</b>.<br>With your support, the Solmar Foundation also runs specific <b>programs to help people</b> with learning disabilities, women in vulnerable situations, underprivileged children and more.</p>",
    "kindness_title_work" => "Kindness is a gift that <span>brings joy to many</span>",
    "confirm_donation_work" => "Confirm Your Donation at Check-Out.<span>Make a tax-deductible online donation.</span>",
    //help_us_help
    "first_title_help_us" => "<h1>Help Us Help</h1><p class='principal-text'>Your support makes it possible for the Solmar Foundation to help the work of various non-profit associations like:</p>",
    "central_text_work" => "<p>During your stay at this Solmar Resort, you can make a voluntary donation <span>that will be added to your bill. There is an optional charge of Ten Dollars $10 per week.</span></p><p>That small donation goes a long way!</p><p>If you want to become a sponsor for one of our programs, you can make a tax-deductible online donation so you too can help improve the lives of the people of Los Cabos wherever you are!.</p>",
    "confirm_donation_help" => "<h1>Confirm Your Donation at Check-Out.</h1>",
    //helping_hands
    "first_title_helping" => "<h1>Join to Helping Hands Tour</h1><p><span>You Can Meet The People</span><span>You Are Helping With Your Donation</span></p><div class='two-cols-text'>
    <div>
        <p class='text-justify'>
            See on this two-hour tour, you will see first-hand the significant impact your donation has upon families. Solmar Foundation and the associations we sponsor in action. This two-hour tour will warm your heart.
        </p>
        <p class='text-justify'>
            Sign up today at the Concierge desk and join us in the free tour that leaves every Wednesday at 9am from the Playa Grande Resort & Grand Spa lobby. The tour is free and space is limited. Ask for availability during rainy season and Holidays.
        </p>
        <p>
            Experience the life of a volunteer and see how your donations impact the lives of many for the betterst.
        </p>
    </div>
    <div>
        <div class='yellow-container'>
            <h1>
                Pack with Purpose
            </h1>
            <p>
                As you prepare your next trip to Los Cabos, remember that there are thousands of families that need your help.<br>One of our programs helps children stay in school. Help this program by including a few school supplies in your luggage, such, notebooks, pencils, pens, backpacks, pencil cases, sharpeners, erasers, and rulers, you can help create a better learning environment and encourage children to further their education. all the basics are a luxury for some of these children. Your donation will encourage them to stay in school.
            </p>
        </div>
    </div>
</div>
<div class='final-paragraph'>
    <p>
        Stop by the Solmar Foundation office for more details.
    </p>
</div>",
"transform_world_helping" => "Help that transforms <span>their world</span>",
"confirm_donate_helping" => "A gift of gently used clothing is always reason for celebration. Something that is always needed is underwear. New underwear for women and children is easy to pack and there are many families that will thank you for your kindness.",
"contact_us_connect" => "Contact Us",
"name_form_connect" => "Name",
"email_form_connect" => "Email",
"phone_form_connect" => "Phone",
"message_form_connect" => "Message",
"submit_form_connect" => "Send Your Message",
"title_connect" => "Connect",
"confirm_donation_connect" => "Confirm Your Donation<span>at Check-Out.</span>",
"error_404" => "Page Not Found",
"confirm_message" => "<h1>Your message was sent!</h1><h2>We will contact you soon</h2><p>If you want to send another message, clic <a href='/connect'>here</a></p>",
//donate
"info_donate" => "<div>
                <h1>Wire Transfer</h1>
                <p>
                    Bank: Banco Santander México SA<br>
                    Beneficiary Bank: Fundación Solmar AC
                </p>
                <p>
                    <span>Mexican Pesos (MN)</span>
                    Account Number: 65502980417<br>
                    Clabe: 014041655029804179
                </p>
                <p>
                    <span>United States Dollar (USD)</span>
                    Account Number: 82500539844<br>
                    Clabe: 014041825005398442
                </p>
            </div>
            <div>
                <h1>Mail a Check</h1>
                <p>
                    Make checks payable to: Fundación Solmar AC<br>
                    Mail México checks to:
                </p>
                <p>
                    C.P. Haydee Olvera Minjarez<br>
                    Depto Vicepresidencia<br>
                    Av. Solmar no. 1, interior Hotel Playa Grande, Colonia Centro, C.P. 23450<br>
                    Cabo San Lucas, Baja California Sur.
                </p>
            </div>
            <div>
                <h1>Donate Online</h1>
                <p>For a tax deducible donation please click here to donate to the Solmar Fundation fund by ICF.</p>
            </div>",
    "button_show_details_blog" => "Read More",
);