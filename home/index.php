<?php
include "../controllers/config.php";
if (isset($_POST["lang"])) {
    $lang = $_POST["lang"];
    if (!empty($lang)) {
        $_SESSION["lang"] = $lang;
    }
}

if (isset($_SESSION["lang"])) {
    $lang = $_SESSION["lang"];
    require "../lang/" . $lang . ".php";
} else {
    require "../lang/en.php";
}

$title = $lang["title_home"];
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <?php include '../layouts/head.template.php'; ?>
    <script>
        $(document).ready(function() {
            $('#social-media-modal').modal('show');
        });
    </script>
</head>

<body class="home">

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0&appId=2434472056623480&autoLogAppEvents=1"></script>

    <?php include '../layouts/nav.template.php'; ?>

    <main id="home">
        <?php include '../layouts/modals.template.php'; ?>
        <div class="principal-container">
            <div class="principal-image">
                <img src="<?= BASE_PATH ?>static/img/home/first-image.jpg" alt="">
            </div>
        </div>
        <div class="about-container container">
            <div>
                <h5 class="text-center"><?= $lang["first_title_home"] ?></h5>
                <hr>
                <div class="text-video-container two-cols">
                    <div class="about-text">
                        <?= $lang["about_text_home"] ?>
                    </div>
                    <div class="video-container">
                        <div class="video-responsive">
                            <iframe src="https://www.youtube.com/embed/17rJt3n_WYk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="donations-become-container special-text-img parallax-effect">
            <div class="over-title">
                <img class="left-quote" src="<?= BASE_PATH ?>static/img/quote-char-yellow.png" alt="">
                <img class="right-quote" src="<?= BASE_PATH ?>static/img/quote-char-yellow.png" alt="">
                <h1>
                    <?= $lang["donation_become_smiles_home"] ?>
                </h1>
            </div>
        </div>
        <div class="commitment-container">
            <div class="container">
                <h1 class="text-center"><?= $lang["commitment_title_home"] ?></h1>
                <div class="text-img-container two-cols">
                    <div class="img-container">
                        <div class="img-responsive">
                            <img src="<?= BASE_PATH ?>static/img/home/commitment-img.jpg" alt="">
                        </div>
                    </div>
                    <div class="commitment-text">
                        <?= $lang["commitment_text_home"] ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="gallery-container container">
            <div>
                <div class="hidden-xs first-gallery row-gallery">
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/help-us-help.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/helping.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/usaer22.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/helping-to-kid.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/help-us-help.jpg" alt="">
                    </div>
                </div>
                <div class="second-gallery">
                    <div class="text-over-img purpose-link scale-img">
                        <a href="">
                            <img src="<?= BASE_PATH ?>static/img/home/purpose.jpg" alt="">
                            <span>
                                <span>
                                    <?= $lang["purpose_home_link"] ?>
                                </span>
                            </span>
                        </a>
                    </div>
                    <div class="two-elements">
                        <div class="special-back">
                            <div>
                                <span>
                                    <?= $lang["little_help_home_link"] ?>
                                </span>
                            </div>
                        </div>
                        <div>
                            <img src="<?= BASE_PATH ?>static/img/home/girl-and-bunny.jpg" alt="">
                        </div>
                    </div>
                    <div class="text-over-img help-link scale-img">
                        <a href="<?= BASE_PATH ?>help-us-help">
                            <img src="<?= BASE_PATH ?>static/img/home/help-us-help.jpg" alt="">
                            <span>
                                <span>
                                    <?= $lang["help_us_home_link"] ?>
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="third-gallery three-cols">
                    <div>
                        <div class="two-img">
                            <div><img src="<?= BASE_PATH ?>static/img/home/helicopter-child.jpg" alt=""></div>
                            <div><img src="<?= BASE_PATH ?>static/img/home/thank-you-board.jpg" alt=""></div>
                        </div>
                        <div class="one-img scale-img">
                            <a href="<?= BASE_PATH ?>work">
                                <img src="<?= BASE_PATH ?>static/img/home/work-together.jpg" alt="">
                                <span>
                                    <?= $lang["work_home_link"] ?>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div>
                        <div class="first-img">
                            <img src="<?= BASE_PATH ?>static/img/quote-char-yellow.png" alt="">
                        </div>
                        <div class="second-img">
                            <img src="<?= BASE_PATH ?>static/img/quote-char-yellow.png" alt="">
                        </div>
                        <p>
                            <?= $lang["mission_home"] ?>
                        </p>
                    </div>
                    <div>
                        <div class="one-img scale-img">
                            <a href="<?= BASE_PATH ?>helping-hands">
                                <img src="<?= BASE_PATH ?>static/img/home/helping-hands.jpg" alt="">
                                <span>
                                    <?= $lang["helping_hands_home_link"] ?>
                                </span>
                            </a>
                        </div>
                        <div class="two-img">
                            <div><img src="<?= BASE_PATH ?>static/img/home/smile-kid.jpg" alt=""></div>
                            <div><img src="<?= BASE_PATH ?>static/img/home/hi-five.jpg" alt=""></div>
                        </div>
                    </div>
                </div>
                <div class="hidden-xs four-gallery row-gallery">
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/logo-draw.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/paz-lemo.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/sit-kid.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/children-eating.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/dog-on-car.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?= BASE_PATH ?>static/img/home/carrera-con-colores.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="solmar-actions">
            <h1><?= $lang["spare_title_home"] ?></h1>
            <div>
                <!-- <div class="special-border-top">
                <img src="<?= BASE_PATH ?>static/img/border-imgs-top.png" alt="">
            </div> -->
                <div class="solmar-actions-backgruond">
                    <img src="<?= BASE_PATH ?>static/img/home/backg-solmar-actions.png" alt="">
                </div>
                <!-- <div class="special-border-bottom">
                <img src="<?= BASE_PATH ?>static/img/border-imgs-bottom-1.png" alt="">
            </div> -->
                <div class="text-cols">
                    <?= $lang["actions_home"] ?>
                </div>
            </div>
        </div>
        <div class="our-work-container container">
            <div class="text-fb-container two-cols">
                <div class="work-text">
                    <h1>Our Work Has Earned</h1>
                    <div>
                        <div class="bullets-text">
                            <img src="<?= BASE_PATH ?>static/img/bullet-green.png" alt="">
                            <p><?= $lang["our_work_home1"] ?></p>
                        </div>
                        <div class="bullets-text">
                            <img src="<?= BASE_PATH ?>static/img/bullet-red.png" alt="">
                            <p><?= $lang["our_work_home2"] ?></p>
                        </div>
                        <div class="bullets-text">
                            <img src="<?= BASE_PATH ?>static/img/bullet-violet.png" alt="">
                            <p><?= $lang["our_work_home3"] ?></p>
                        </div>
                        <div class="bullets-text">
                            <img src="<?= BASE_PATH ?>static/img/bullet-yellow.png" alt="">
                            <p><?= $lang["our_work_home4"] ?></p>
                        </div>
                        <div class="bullets-text">
                            <img src="<?= BASE_PATH ?>static/img/bullet-turquoise.png" alt="">
                            <p><?= $lang["our_work_home5"] ?></p>
                        </div>
                    </div>
                </div>
                <div class="fb-widget-container">
                    <div class="fb-widget-responsive">
                        <div class="fb-page" data-href="https://www.facebook.com/fundacion.solmar/" data-tabs="timeline" data-width="" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/fundacion.solmar/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/fundacion.solmar/">Fundación Solmar</a></blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="confirm-donate-container parallax-effect">
            <div class="over-title">
                <div class="over-title-elements">
                    <h1>
                        <?= $lang["confirm_donation_home"] ?>
                    </h1>
                    <div class="custom-button">
                        <a href="<?= BASE_PATH ?>donate"><?= $lang["donate_now_home"] ?></a>
                    </div>
                </div>

            </div>
        </div>
    </main>
</body>

<?php include '../layouts/footer.template.php'; ?>

<?php include '../layouts/modals.template.php'; ?>

<?php include '../layouts/scripts.template.php'; ?>