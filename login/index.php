<?php
include "../controllers/config.php";
if (isset($_POST["lang"])) {
    $lang = $_POST["lang"];
    if (!empty($lang)) {
        $_SESSION["lang"] = $lang;
    }
}

if (isset($_SESSION["lang"])) {
    $lang = $_SESSION["lang"];
    require "../lang/" . $lang . ".php";
} else {
    require "../lang/en.php";
}

$title = $lang["title_donate"];
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <?php include '../layouts/head.template.php'; ?>
    <meta name="robots" content="index,nofollow">
</head>

<body class="login">

    <?php include '../layouts/nav.template.php'; ?>

    <main id="login">
        <div class="container">
            <h1></h1>
            <!-- NOTIFICATION -->            
			<?php if (isset($_SESSION['status']) && $_SESSION['status']=="error"): ?>  
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
			  <strong>Error!</strong> <?= $_SESSION['message'] ?>.
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<?php unset($_SESSION['status']); ?>
			<?php endif ?>
            
            <form method="POST" action="../controllers/AuthController.php">
                <div class="form-group">
                    <label for="exampleInputEmail1">Usuario</label>
                    <input type="text" name="usuario" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Contraseña</label>
                    <input type="password" name="password" class="form-control" id="exampleInputPassword1" required="">
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                </div>
                <input type="hidden" name="token" value="<?= $_SESSION['token'] ?>">
                <input type="hidden" name="action" id="action" value="login">
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </main>
</body>

<?php include '../layouts/footer.template.php'; ?>

<?php include '../layouts/scripts.template.php'; ?>