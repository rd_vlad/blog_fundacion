<?php

include_once "config.php";

if (isset($_POST) && isset($_POST['action'])) {
    if ($_POST['token'] == $_SESSION['token']) { 

        $authController = new AuthController();

        switch ($_POST['action']) {
            case 'login':
                $usuario = strip_tags($_POST['usuario']);
                $password = strip_tags($_POST['password']);
        
                $authController -> login($usuario, $password);
                break;

            case 'logout':
                $authController -> logout();
                break;
        }

    }else{
        $respuesta = array(
            'status' => "error",
            'message' => "Sin autorización"
        );
        echo json_encode($respuesta);
    }
}

Class AuthController{
    public function login($usuario, $password){

        if($usuario != "" && $password != ""){
               if($usuario === "usuario" && $password === "123"){


                // session_start();

                $_SESSION['usuario'] = $usuario;

                header('Location: ' . BASE_PATH . '/panel');
               }else{
                $_SESSION['status'] = "error";
                $_SESSION['message'] = "Los datos ingresados son incorrectos";

                header('Location: ' . $_SERVER['HTTP_REFERER']);
               }
        }else{

            $_SESSION['status'] = "error";
            $_SESSION['message'] = "Verifique la información enviada";

            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }
    public function logout(){
        unset($_SESSION['usuario']);
        header('Location: ' . BASE_PATH);
    }
}

?>