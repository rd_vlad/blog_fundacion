<?php 

	include_once "config.php";
	include_once "connectionController.php";

	if (isset($_POST) && isset($_POST['action'])) {
		if ($_POST['token'] == $_SESSION['token']) { 

			$blogController = new BlogController();

			switch ($_POST['action']) {
				case 'store':
					
					$titulo_es  = strip_tags($_POST['titulo_es']);
					$titulo_en  = strip_tags($_POST['titulo_en']);
					$slug  = strip_tags($_POST['slug']);
					$template  = strip_tags($_POST['template']);
					$parrafo_1_es  = strip_tags($_POST['parrafo_1_es']);
					$parrafo_2_es  = strip_tags($_POST['parrafo_2_es']);
					$parrafo_3_es  = strip_tags($_POST['parrafo_3_es']);
					$parrafo_4_es  = strip_tags($_POST['parrafo_4_es']);
					$parrafo_1_en  = strip_tags($_POST['parrafo_1_en']);
					$parrafo_2_en  = strip_tags($_POST['parrafo_2_en']);
					$parrafo_3_en  = strip_tags($_POST['parrafo_3_en']);
					$parrafo_4_en  = strip_tags($_POST['parrafo_4_en']);
					$imagen_1 = $_FILES['imagen_1'];
					$imagen_2 = $_FILES['imagen_2'];
					$imagen_3 = $_FILES['imagen_3'];

					$blogController->store(
						$titulo_es,
						$titulo_en,
						$slug,
						$template,
						$parrafo_1_es,
						$parrafo_2_es,
						$parrafo_3_es,
						$parrafo_4_es,
						$parrafo_1_en,
						$parrafo_2_en,
						$parrafo_3_en,
						$parrafo_4_en,
						$imagen_1,
						$imagen_2,
						$imagen_3);

				break;

				case 'update':
					$titulo_es  = strip_tags($_POST['titulo_es']);
					$titulo_en  = strip_tags($_POST['titulo_en']);
					$slug  = strip_tags($_POST['slug']);
					$template  = strip_tags($_POST['template']);
					$parrafo_1_es  = strip_tags($_POST['parrafo_1_es']);
					$parrafo_2_es  = strip_tags($_POST['parrafo_2_es']);
					$parrafo_3_es  = strip_tags($_POST['parrafo_3_es']);
					$parrafo_4_es  = strip_tags($_POST['parrafo_4_es']);
					$parrafo_1_en  = strip_tags($_POST['parrafo_1_en']);
					$parrafo_2_en  = strip_tags($_POST['parrafo_2_en']);
					$parrafo_3_en  = strip_tags($_POST['parrafo_3_en']);
					$parrafo_4_en  = strip_tags($_POST['parrafo_4_en']);
					$imagen_1 = $_FILES['imagen_1'];
					$imagen_2 = $_FILES['imagen_2'];
					$imagen_3 = $_FILES['imagen_3'];
					$id = strip_tags($_POST['id']);
					
					$blogController->update(
						$titulo_es,
						$titulo_en,
						$slug,
						$template,
						$parrafo_1_es,
						$parrafo_2_es,
						$parrafo_3_es,
						$parrafo_4_es,
						$parrafo_1_en,
						$parrafo_2_en,
						$parrafo_3_en,
						$parrafo_4_en,
						$imagen_1,
						$imagen_2,
						$imagen_3,
						$id);

				break; 

				case 'remove':
					$id = strip_tags($_POST['blog_id']);
					echo json_encode($blogController->remove($id));
				break;
			}

		}else{
			$respuesta = array(
				'status' => "error",
				'message' => "Sin autorización"
			);
			echo json_encode($respuesta);
		}
	}

	Class BlogController
	{
		function get($order)
		{
			$conn = connect();
			if (!$conn->connect_error) {
				
				$query = "SELECT * FROM blogs $order";
				$prepared_query = $conn->prepare($query);
				
				$prepared_query->execute();

				$results = $prepared_query->get_result();
				$blogs = $results->fetch_all(MYSQLI_ASSOC);

				if (count($blogs)>0) {
					return $blogs;
				}else
					return array();
				
			}else
				return array();
		}

		function show($slug){
			$conn = connect();
			if (!$conn->connect_error) {
				
				$query = "SELECT * FROM blogs WHERE slug = '$slug'";
				$prepared_query = $conn->prepare($query);
				$prepared_query->execute();

				$results = $prepared_query->get_result();
				$blog = $results->fetch_all(MYSQLI_ASSOC);

				if (count($blog)>0) {
					return $blog;
				}else
					return array();
				
			}else
				return array();
		}

		public function store(
			$titulo_es,
			$titulo_en,
			$slug,
			$template,
			$parrafo_1_es,
			$parrafo_2_es,
			$parrafo_3_es,
			$parrafo_4_es,
			$parrafo_1_en,
			$parrafo_2_en,
			$parrafo_3_en,
			$parrafo_4_en,
			$imagen_1,
			$imagen_2,
			$imagen_3)
		{
			$conn = connect();
			if (!$conn->connect_error) {

				if($titulo_es !="" && 
				   $titulo_en !="" && 
				   $slug !="" && 
				   $template !="" &&
				   $parrafo_1_es !="" && 
				   $parrafo_2_es !="" &&  
				   $parrafo_1_en !="" && 
				   $parrafo_2_en !=""){

					$fecha = date("Y-m-d");
   
					//Crea la ruta destino para la imagen
					//$directorio = $_SERVER['DOCUMENT_ROOT'].'/blog_solmar/static/img/blogs/';  
					$directorio = '../static/img/blogs/';  
					
					if($imagen_1['error'] == 0){
						
						$basename_1 = basename($imagen_1['name']);
						list($base_1, $extension_1) = explode('.', $basename_1);
						$newName_1 = implode('.', [$slug.'_'.time().'_'.'img1', $extension_1]);
					}
					if($imagen_2['error'] == 0){
						
						$basename_2 = basename($imagen_2['name']);
						list($base_2, $extension_2) = explode('.', $basename_2);
						$newName_2 = implode('.', [$slug.'_'.time().'_'.'img2', $extension_2]);
					}
					if($imagen_3['error'] == 0){
						
						$basename_3 = basename($imagen_3['name']);
						list($base_3, $extension_3) = explode('.', $basename_3);
						$newName_3 = implode('.', [$slug.'_'.time().'_'.'img3', $extension_3]);
					}

					$query = "insert into blogs (titulo_es,titulo_en,slug,template,parrafo_1_es,parrafo_2_es,parrafo_3_es,parrafo_4_es,parrafo_1_en,parrafo_2_en,parrafo_3_en,parrafo_4_en,imagen_1,imagen_2,imagen_3, fecha) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					$prepared_query = $conn->prepare($query);
					$prepared_query->bind_param('ssssssssssssssss',$titulo_es, $titulo_en, $slug,$template,$parrafo_1_es,$parrafo_2_es,$parrafo_3_es,$parrafo_4_es,$parrafo_1_en,$parrafo_2_en,$parrafo_3_en,$parrafo_4_en,$newName_1,$newName_2,$newName_3, $fecha);

					if ($prepared_query->execute()) {

						move_uploaded_file($imagen_1['tmp_name'], $directorio . $newName_1);
						move_uploaded_file($imagen_2['tmp_name'], $directorio . $newName_2);
						move_uploaded_file($imagen_3['tmp_name'], $directorio . $newName_3);
						
						$_SESSION['status'] = "success";
						$_SESSION['message'] = "El registro se ha guardado correctametne";

						// $nombre_img_1 = $slug . '_' . $mysqli -> insert_id . '_' . 'blog';  
						// move_uploaded_file($imagen_1['tmp_name'], $directorio . $imagen_1['name']);

						header('Location: ' . $_SERVER['HTTP_REFERER']);
					}else{

						$_SESSION['status'] = "error";
						$_SESSION['message'] = "El registro no se ha guardado";

						header('Location: ' . $_SERVER['HTTP_REFERER']);
					}

				}else{

					$_SESSION['status'] = "error";
					$_SESSION['message'] = "Verifique la información enviada";

					header('Location: ' . $_SERVER['HTTP_REFERER']);
				}

			}else{

				$_SESSION['status'] = "error";
				$_SESSION['message'] = "Error durante la conexión";

				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
		}

		public function update(
			$titulo_es,
			$titulo_en,
			$slug,
			$template,
			$parrafo_1_es,
			$parrafo_2_es,
			$parrafo_3_es,
			$parrafo_4_es,
			$parrafo_1_en,
			$parrafo_2_en,
			$parrafo_3_en,
			$parrafo_4_en,
			$imagen_1,
			$imagen_2,
			$imagen_3,
			$id)
		{
			$conn = connect();
			if (!$conn->connect_error) {

				if(
					$titulo_es != "" && 
					$titulo_en != "" && 
					$slug != "" && 
					$template != "" &&
					$id != ""){

					$fecha = date("Y-m-d");
					//$directorio = $_SERVER['DOCUMENT_ROOT'].'/blog_solmar/static/img/blogs/';  
					$directorio = '../static/img/blogs/'; 

					if($imagen_1['error'] == 0){

						$basename_1 = basename($imagen_1['name']);
						list($base_1, $extension_1) = explode('.', $basename_1);
						$newName_1 = implode('.', [$slug.'_'.time().'_'.'img1', $extension_1]);
						$query_img_1 = "update blogs set imagen_1 = ? where id = ?";
						$prepared_query_1 = $conn->prepare($query_img_1);
						$prepared_query_1->bind_param('si', $newName_1, $id);
					}
					if($imagen_2['error'] == 0){

						$basename_2 = basename($imagen_2['name']);
						list($base_2, $extension_2) = explode('.', $basename_2);
						$newName_2 = implode('.', [$slug.'_'.time().'_'.'img2', $extension_2]);
						$query_img_2 = "update blogs set imagen_2 = ? where id = ?";
						$prepared_query_2 = $conn->prepare($query_img_2);
						$prepared_query_2->bind_param('si', $newName_2, $id);
					}
					if($imagen_3['error'] == 0){
						
						$basename_3 = basename($imagen_3['name']);
						list($base_3, $extension_3) = explode('.', $basename_3);
						$newName_3 = implode('.', [$slug.'_'.time().'_'.'img3', $extension_3]);
						$query_img_3 = "update blogs set imagen_3 = ? where id = ?";
						$prepared_query_3 = $conn->prepare($query_img_3);
						$prepared_query_3->bind_param('si', $newName_3, $id);
					}


					$query = "update blogs set titulo_es = ?, titulo_en = ?, slug = ?, template = ?, parrafo_1_es = ?, parrafo_2_es = ?, parrafo_3_es = ?, parrafo_4_es = ?, parrafo_1_en = ?, parrafo_2_en = ?, parrafo_3_en = ?, parrafo_4_en = ?, fecha = ? where id = ?";

					$prepared_query = $conn->prepare($query);
					$prepared_query->bind_param('sssssssssssssi',$titulo_es, $titulo_en, $slug,$template,$parrafo_1_es,$parrafo_2_es,$parrafo_3_es,$parrafo_4_es,$parrafo_1_en,$parrafo_2_en,$parrafo_3_en,$parrafo_4_en,$fecha,$id);

					if ($prepared_query->execute()) {

						if(isset($prepared_query_1) && $prepared_query_1->execute()){
							move_uploaded_file($imagen_1['tmp_name'], $directorio . $newName_1);
						}
						if(isset($prepared_query_2) && $prepared_query_2->execute()){
							move_uploaded_file($imagen_2['tmp_name'], $directorio . $newName_2);
						}
						if(isset($prepared_query_3) && $prepared_query_3->execute()){
							move_uploaded_file($imagen_3['tmp_name'], $directorio . $newName_3);
						}
						//var_dump($_POST);
						
						$_SESSION['status'] = "success";
						$_SESSION['message'] = "El registro se ha actualizado correctametne";

						header('Location: ' . $_SERVER['HTTP_REFERER']);
					}else{

						$_SESSION['status'] = "error";
						$_SESSION['message'] = "El registro no se ha actualizado";

						header('Location: ' . $_SERVER['HTTP_REFERER']);
					}

				}else{

					$_SESSION['status'] = "error";
					$_SESSION['message'] = "Verifique la información enviada";

					header('Location: ' . $_SERVER['HTTP_REFERER']);
				}

			}else{

				$_SESSION['status'] = "error";
				$_SESSION['message'] = "Error durante la conexión";

				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}
		}

		public function remove($id)
		{
			$conn = connect();
			if (!$conn->connect_error) {

				if($id !=""){ 

					$query = "delete from blogs where id = ?";
					$prepared_query = $conn->prepare($query);
					$prepared_query->bind_param('i',$id);

					if ($prepared_query->execute()) { 

						$respuesta = array(
							'status' => "success",
							"id" => $id,
							'message' => "El registro se ha eliminado correctametne"
						);
						return $respuesta;
					}else{

						$respuesta = array(
							'status' => "error",
							'message' => "El registro no se ha eliminado"
						);
						return $respuesta; 
					}

				}else{

					$respuesta = array(
						'status' => "error",
						'message' => "Verifique la información enviada"
					);
					return $respuesta; 
				}

			}else{

				$respuesta = array(
					'status' => "error",
					'message' => "Error durante la conexión"
				);
				return $respuesta;  
			}
		}	
	}

?>